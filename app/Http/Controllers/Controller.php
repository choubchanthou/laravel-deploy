<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Exceptions\CustomException;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @author choub chanthou
     * @description response success all function
     */
    public function responseSuccess($message, $result)
    {
        return [
            "status" => true,
            "message" => $message,
            "result" => $result
        ];
    }

    /**
     * @author choub chanthou
     * @description delete by id from any model
     */
    public function delete($id)
    {
        $this->checkModel();
        $model = $this->getModel();
        return $id;     
    }

    /**
     * @author choub chanthou
     * @description response error all function 
     */
    public function responseError($message, $result)
    {
        return [
            "status" => false,
            "message" => $message,
            "result" => $result
        ];
    }

    /**
     * @author choub chanthou
     * @override
     * @description must be override
     */
    abstract protected function getModel();

    /**
     * @author choub chanthou
     * @description update by field id from any model
     */
    public function update($id, $data)
    {
        $model = $this->getModel();
        $model->find($id);
        $result = $model->update($data);

        return $result;
    }

    /**
     * @author choub chanthou
     * @description create new from any model
     */
    public function create($data)
    {
        $model = $this->getModel();
        $model->fill($data);
        $result = $model->save();
        return $result;
    }

    /**
     * @author choub chanthou
     * @description multiple create from any model
     */
    public function createAll($data)
    {
        $createError = [];
        $createSuccess = [];

        foreach($data as $key => $value){
            $create = $this->create($value);

            if($create){
                array_push($createError, $value);
            }else {
                array_push($createSuccess, $value);
            }
        }
        return [
            "error" => $createError,
            "success" => $createSuccess
        ];
    }

    /**
     * @author choub chanthou
     * @description update all by each id of row from any model
     */
    public function updateAll($data)
    {
        $updateError = [];
        $updateSuccess = [];

        foreach($data as $key => $value){
            $id = $value['id'];
            $value = $this->removeKeyId($value);
            $update = $this->update($id, $value);

            if ($update){
                array_push($updateSuccess, $id);
            }else {
                array_push($updateError, $id);
            }
        }

        return [
            "error" => $updateError,
            "success" => $updateSuccess
        ];
    }

    /**
     * @author choub chanthou
     * @description get data filter by any field id from any model
     */
    public function getById($id)
    {
        try{
            $model = $this->getModel();
            $result = $model->find($id);

            return response()->json($this->responseSuccess($message, $result));
        }catch (Exception $ex){
            return response()->json($this->responseError($message . "\n" . $ex, $result));
        }
    }

    /**
     * @author choub chanthou
     * @description get data filter by any field from any model
     */
    public function getByField($key, $value)
    {
        $model = $this->getModel();
    }

    /**
     * @author choub chanthou
     * @description get all data from any model
     */
    public function getAll()
    {
        $model = $this->getModel();
        $result = $model->all();
        return $result;
    }

    /**
     * @author choub chanthou
     * @description remove object by key id
     */
    private function removeKeyId($data, $key = 'id'){
        unset($data[$key]);

        return $data;
    }
}
